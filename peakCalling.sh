#!/usr/bin/bash
source ~/Fresh_Scripts/./FileParser_v2.sh


function macsSingleSample() {
	module load bbc/macs2/macs2-2.2.7.1

	base=$(pwd)
	[ -d MACS_SingleSample ] || mkdir MACS_SingleSample

	Sample_input -l $1 -t $2 -f $3
	macs2 callpeak -t ${Sline} --outdir ${base}/MACS_SingleSample -n ${SAMPLE}
	module purge
}

# For cut&Run data
function seacR() {
	# Arguments $4, $5, $6 are for SEACR_1.3.sh params
	base=$(pwd)
	[ -d SEACR_peakCalling ] || mkdir SEACR_peakCalling

	module load bbc/bedtools/bedtools-2.29.2
	Sample_input -l $1 -t $2 -f $3

	# Creating bed file from bam (On SEACR Website)
	bedtools bamtobed -bedpe -i ${Sline} > ${base}/SEACR_peakCalling/${SAMPLE}.bed
	awk '$1==$4 && $6-$2 < 1000 {print $0}' ${base}/SEACR_peakCalling/${SAMPLE}.bed > ${base}/SEACR_peakCalling/${SAMPLE}_clean.bed
	cut -f 1,2,6 ${base}/SEACR_peakCalling/${SAMPLE}_clean.bed | sort -k1,1 -k2,2n -k3,3n > ${base}/SEACR_peakCalling/${SAMPLE}_fragments.bed
	bedtools genomecov -bg -i ${base}/SEACR_peakCalling/${SAMPLE}_fragments.bed -g /home/nick.pierce/szabo-secondary/pierce_nick/Genomes/chromSizes/mm10.chrom.sizes > ${base}/SEACR_peakCalling/${SAMPLE}_fragments.bedgraph

	# Running SEACR
	/home/nick.pierce/Tools/SEACR/./SEACR_1.3.sh ${base}/SEACR_peakCalling/${SAMPLE}_fragments.bedgraph $4 $5 $6 ${base}/SEACR_peakCalling/${SAMPLE}
	rm -r ${base}/SEACR_peakCalling/${SAMPLE}.bed ${base}/SEACR_peakCalling/${SAMPLE}_clean.bed ${base}/SEACR_peakCalling/${SAMPLE}_fragments.bed
	module purge
}

function sicerPeaks() {
	base=$(pwd)
	[ -d SICER_peakCalling ] || mkdir SICER_peakCalling

	source /home/nick.pierce/szabo-secondary/pierce_nick/anaconda2/etc/profile.d/conda.sh
	conda activate sicer2

	Sample_input -l $1 -t $2 -f $3	
	sicer -t ${Sline} -s mm10 -w $4 -fdr $5 -o ${base}/SICER_peakCalling/
}
