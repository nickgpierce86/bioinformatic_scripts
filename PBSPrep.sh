#!/usr/bin/bash


PBSPrepUsage() { echo "Usage $0 -N [job_name] -o [name of stdout file] -e [name of stderr file] -l [nodes] -p [ppn] -w [walltime] -t [array_job]" 1>&2; exit 1; }

while getopts ":N:o:e:l:p:w:t:" opt; do
  	case "${opt}" in
		N) jobName=${OPTARG} ;;
		o) stdOut=${OPTARG} ;;
		e) stdErr=${OPTARG} ;;
		l) nodes=${OPTARG} ;;
		p) ppn=${OPTARG} ;;
        w) wallTime=${OPTARG} ;;
        t) arrayJob=${OPTARG} ;;
		:) 
			echo "Error: -${OPTARG} requires argument"
			exit 1 ;;
		*) PBSPrepUsage ;;
	esac
done
shift $((OPTIND -1))

{
    echo -e "#PBS -N ${jobName}"
    echo -e "#PBS -o ${PBS_O_WORKDIR}/std_out/${stdOut}"
    echo -e "#PBS -e ${PBS_O_WORKDIR}/std_err/${stdErr}"
    echo -e "#PBS -l nodes=${nodes}:ppn=${ppn}"
    echo -e "#PBS -l walltime=${wallTime}"
    if [[ -n ${arrayJob} ]]; then
        echo -e "#PBS -t ${arrayJob}"
    fi
    echo -e "#PBS -d ${PBS_O_WORKDIR}"
} > ${PBS_O_WORKDIR}/${jobName}.pbs







