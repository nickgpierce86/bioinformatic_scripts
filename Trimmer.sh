#!/usr/bin/bash

# USAGE
#$1:directory of Samples_file.txt
#$2:single or paired
#$3:trimming method: trim_default; trim_clipR2
#$4: Number of bp to trim

source ~/Fresh_Scripts/./FileParser_v2.sh

function Galore_Trim {
	base=$(pwd)
	[ -d trimmed_fastq ] || mkdir trimmed_fastq
	
	# Reading In Files
	Sample_input -l $1 -t $2 -f $3

	# Running Trim-galore
	if [ $2 == "single" ] && [ $4 == "trim_default" ]; then
		trim_galore ${extra_args} --fastqc --gzip -o ${base}/trimmed_fastq ${Sline}
	elif [ $2 == "paired" ] && [ $4 == "trim_default" ]; then
		trim_galore --paired --fastqc --gzip -o ${base}/trimmed_fastq ${Sline}
	elif [ $2 == "paried" ] && [ $4 == "trim_clipR2" ]; then
		trim_galore --clip_R2 $5 --paired --fastqc --gzip -o ${base}/trimmed_fastq ${Sline}
	else
		echo "Trimming failed."
	fi
}
