#!/usr/bin/bash

module load bbc/sratoolkit/sratoolkit-2.10.0
source ~/Fresh_Scripts/./FileParser.sh

########################
# Function
########################
sraDownloadReads() {
 	base=$(pwd)
	
	#------------------------------------
	# Arguments for sraDownload function
	#------------------------------------
	USAGE () { echo "Downloads SRA Reads using input text file"
	"Usage $0 -t [paired,single] -f Samples_file.txt"
	1>&2
	exit 1
	}

	while getopts ":t:f:" opt; do
		case "${opt}" in
			t) TYPE=${OPTARG} ;;
			f) FILE=${OPTARG} ;;
			:)
				echo "ERROR: -${OPTARG} requires argument"
				exit 1 ;;
			*) USAGE ;;
		esac
	done
	shift $((OPTIND -1))
	
	#--------------------------------------
	# Parsing File
	#--------------------------------------
	[ -d fastq_files ] || mkdir fastq_files
	Sample_Input ${FILE} single

	#--------------------------------------
	# Downloading SRA and extracting reads
	#--------------------------------------
	prefetch ${Sline} --output-file ${SAMPLE}.sra --output-directory ${base}/SRA/
	
	if [ ${TYPE} == "paired"]; then
		fastq-dump --gzip --outdir ${base}/fastq_files/ --split-files ${base}/SRA/${SAMPLE}.sra
	elif [ ${TYPE} == "single"]; then
		fastq-dump --gzip --outdir ${base}/fastq_files ${base}/SRA/${SAMPLE}.sra
	else
		echo "Error with input sample type"
	fi
}
