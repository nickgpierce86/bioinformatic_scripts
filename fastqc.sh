#!/usr/bin/bash

# Update path to fastqc
myFastQC=<pathToFastqcBin>/./fastqc

##############################
# USAGE
# $1: Location of the files
# $2: Input type; options *fastq.gz; *.bam
#############################

#----------------------------
# Creating output directory
#----------------------------
base=$(pwd)
[ -d FastQC_Res ] || mkdir FastQC_Res

#--------------------------------
# Changing to directory where fastq files are located
#--------------------------------
cd $1

#--------------------------------
# Running Fastqc
#--------------------------------
${myFastQC} -t 4 -o ${base}/FastQC_Res/ $2
